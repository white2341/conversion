window.onload = function(e) {
    populateSelect(document.unitTypeForm.unitTypeList, unitType);
    updateUnitList(document.unitTypeForm.unitTypeList, document.unitForm1.unitList);
    updateUnitList(document.unitTypeForm.unitTypeList, document.unitForm2.unitList);
};

/**
 * This method gets the correct unit list based on the unitType selected
 * and calls populateSelect with results
 * @param unitTypeSelect unitType select object
 * @param unitSelect unitList select object
 */
function updateUnitList(unitTypeSelect, unitSelect) {
    populateSelect(unitSelect, unit[unitTypeSelect.selectedIndex]);
}

/**
 * This method populated a select list with the unit array
 * @param unitSelect select object
 * @param units unit array
 */
function populateSelect(unitSelect, units) {
    unitSelect.length = units.length;
    units.forEach((unit, index) => {
        unitSelect.options[index].text = unit;
    });
}

/**
 * This method builds the necessary parameters to call the util method
 * which returns the converted value
 * @param sourceForm form containing source unit and value
 * @param targetForm form containing destination unit
 */
function convert(sourceForm, targetForm) {
    const unitTypeIndex = document.unitTypeForm.unitTypeList.selectedIndex;
    const sourceIndex = sourceForm.unitList.selectedIndex;
    const targetIndex = targetForm.unitList.selectedIndex;
    const initialValue = sourceForm.unitInput.value;

    targetForm.unitInput.value = getConvertedValue(unitTypeIndex, sourceIndex, targetIndex, initialValue);
}