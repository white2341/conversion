//global variables; could have made const, but this felt more readable
let unitType = [];
let unit = [];
let factor = [];

unitType[0] = "Length";
unit[0] = ["CENTIMETERS (CM)", "INCHES (IN)", "FEET (FT)", "YARDS (YD)"];
factor[0] = [1, 2.54, 30.48, 91.44];

unitType[1] = "Mass/Volume";
unit[1] = ["LITERS (L)", "GALLONS (G)", "POUNDS (LB)", "KILOGRAMS (KG)"];
factor[1] = [1, 3.8, .475, 1];

/**
 * This method converts the unit of measure based on the chosen array index provided
 * @param unitTypeIndex form containing source unit and value
 * @param sourceIndex form containing destination unit
 * @param targetIndex form containing destination unit
 * @param initialValue form containing destination unit
 * @return converted value
 */
function getConvertedValue(unitTypeIndex, sourceIndex, targetIndex, initialValue) {
    const sourceFactor = factor[unitTypeIndex][sourceIndex];
    const targetFactor = factor[unitTypeIndex][targetIndex];

    let value = initialValue;
    value = value * sourceFactor;
    value = value / targetFactor;

    return value;
}